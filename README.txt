
Binary code for Part-based Tracker v1.0-20130912
-----------------------------------------------------------------
Copyright Rui Yao, Qinfeng (Javen) Shi, September 2013.
ruiyao@cumt.edu.cn, qinfeng.shi@ieee.org

This code package is provided for non-commercial academic research use.
Commercial use is strictly prohibited without the author's written
consent.

Please cite the following paper if you use this code package or part of it
in your publication:
[1] Rui Yao, Qinfeng Shi, Chunhua Shen, Yanning Zhang, Anton van den hengel, Part-based Visual Tracking with Online Latent Structural Learning, 
In IEEE Conference on Computer Vision and Pattern Recognition (CVPR 13), Portland, USA, June 25-27, 2013.


-------
General
-------

This distribution includes the executable code for the tracker in [1]. It is written in C++ and uses the OpenCV 2.1. It has been tested on a machine running Windows 7, using Visual Studio 9.0.
	
-----
Usage
-----

1. Prepare a config file called "config.txt" and place it in the same directory as the executable file.

2. Run the executable file "PartTracker_v1.0.exe".

3. The following output should be obtained:

	- during the run - a visual window with the tracking results
	- a result file called "PartTracker_[sequence name]_gt.txt" containing the tracking results in the same directory as the tracking sequence
	

---------
Sequences
---------

Sequences are assumed to be of the format of those used in:

http://www.samhare.net/research/struck
